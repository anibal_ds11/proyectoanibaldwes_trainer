<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{ url('assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/Ncss.css') }}">
    <script type="text/javascript" src="{{ url('assets/bootstrap/js/bootstrap.min.js') }}"></script>
    <title>@yield('titulo')</title>
  </head>
  <body>
      @include('partials.navbar')
   <div class="container-fluid">
     @yield('contenido')
   </div>
  </body>
</html>