@extends("layouts.master")

@section("titulo")
 Página de entrenador Personal
@endsection
@section("contenido") 
<div class="row">
<div class="col-sm-3">
<img src="{{asset('assets/imagenes')}}/{{$cliente->fotoCara}}" style="height:200px"/>
</div>
<div class="col-sm-9">
<h3>{{$cliente->nombre}} -> {{$cliente->especie}}</h3>
<h4>{{$cliente->apellidos}}</h4>
<h4>Tipo: {{$cliente->tipo}}</h4>
<h4>Objetivo: {{$cliente->objetivo}}</h4>
<h4>Datos: {{$cliente->datosImportancia}}</h4>
<h3>Dieta:</h3>
<h5>{{$cliente->getDesayuno($cliente->idComida)}}</h5>
<h5>{{$cliente->getAperitivo($cliente->idComida)}}</h5>
<h5>{{$cliente->getComida($cliente->idComida)}}</h5>
<h5>{{$cliente->getMerienda($cliente->idComida)}}</h5>
<h5>{{$cliente->getCena($cliente->idComida)}}</h5>
<a href="{{url('/clientes/editar')}}/{{$cliente->id}}" class="btn btn-warning" tabindex="-1" role="button" aria-disabled="true">Editar</a>
<a href="{{url('/clientes')}}" class="btn btn-secondary" tabindex="-1" role="button" aria-disabled="true">Volver al listado</a>
</div>
</div>
@endsection
