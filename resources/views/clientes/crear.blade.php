@extends("layouts.master")

@section("titulo")
 Entrenador Personal
@endsection
@section("contenido") 
<div class="row">
 <div class="offset-md-3 col-md-6">
 <div class="card">
 <div class="card-header text-center">
 Añadir cliente
 </div>
 <div class="card-body" style="padding:30px">
 {{-- TODO: Abrir el formulario e indicar el método POST --}}
 <form action ="{{action('ClientesController@postCrear')}}" method="POST" enctype="multipart/form-data">
  {{ csrf_field()}}
 <div class="form-group">
 <label for="nombre">Nombre</label>
 <input type="text" name="nombre" id="nombre" class="form-control">
 </div>
 <div class="form-group">
 <label for="apellidos">Apellidos</label>
 <input type="text" name="apellidos" id="apellidos" class="form-control">
 </div>
 <div class="form-group">
 <label for="objetivo">Objetivo</label>
 <input type="text" name="objetivo" id="objetivo" class="form-control">
 </div>
 <div class="form-group">
 <label for="datosImportancia">Datos de importancia</label>
 <input type="text" name="datosImportancia" id="datosImportancia" class="form-control">
 </div>
 <div class="form-group">
 <label for="tipo">Tipo</label>
 <input type="text" name="tipo" id="tipo" class="form-control">
 </div>
  <div class="form-group">
 <label for="dias">Dias disponibles</label>
 <input type="number" name="dias" id="dias" class="form-control" min="0" max="7">
 </div>
 <h4>Dieta</h4>
 <div class="form-group">
 <label for="desayuno">Desayuno</label>
 <textarea name="desayuno" id="desayuno" class="form-control" rows="3"></textarea>
 </div>
 <div class="form-group">
 <label for="aperitivo">Aperitivo</label>
 <textarea name="aperitivo" id="aperitivo" class="form-control" rows="3"></textarea>
 </div>
 <div class="form-group">
 <label for="comida">Comida</label>
 <textarea name="comida" id="comida" class="form-control" rows="3"></textarea>
 </div>
 <div class="form-group">
 <label for="merienda">Merienda</label>
 <textarea name="merienda" id="merienda" class="form-control" rows="3"></textarea>
 </div>
 <div class="form-group">
 <label for="cena">Cena</label>
 <textarea name="cena" id="cena" class="form-control" rows="3"></textarea>
 </div>
 <div class="form-group">
  <label for="imagen">Imagen</label>
 <input type="file" id="imagen"  name="imagen" class="form-control"></input>
 </div>
 <div class="form-group text-center">
 <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
 Añadir cliente
 </button>
 </div>
  </form>
 </div>
 </div>
 </div>
</div>
@endsection