@extends("layouts.master")

@section("titulo")
 OnlineTrainer
@endsection
@section("contenido") 
<a href="{{url('/clientes/ver')}}/{{$cliente->id}}" class="btn btn-warning" tabindex="-1" role="button" aria-disabled="true">Mostrar</a>
<a href="{{url('/clientes')}}" class="btn btn-secondary" tabindex="-1" role="button" aria-disabled="true">Volver al listado</a>
Editando a {{$cliente->nombre}} /ID: {{$cliente->id}}
<div class="row">
 <div class="offset-md-3 col-md-6">
 <div class="card">
 <div class="card-header text-center">
 Editar cliente 
 </div>
 <div class="card-body" style="padding:30px">
 {{-- TODO: Abrir el formulario e indicar el método POST --}}
 <form action ="{{url('/clientes/editar')}}/{{$cliente->id}}" method="POST" enctype="multipart/form-data">
  {{ csrf_field()}}
 <div class="form-group">
 <label for="nombre">Nombre</label>
 <input type="text" name="nombre" id="nombre" class="form-control" value="{{$cliente->nombre}}">
 </div>
 <div class="form-group">
 <label for="apellidos">Apellidos</label>
 <input type="text" name="apellidos" id="apellidos" class="form-control" value="{{$cliente->apellidos}}">
 </div>
 <div class="form-group">
 <label for="objetivo">Objetivo</label>
 <input type="text" name="objetivo" id="objetivo" class="form-control" value="{{$cliente->objetivo}}">
 </div>
 <div class="form-group">
 <label for="datosImportancia">Datos de importancia</label>
 <input type="text" name="datosImportancia" id="datosImportancia" class="form-control" value="{{$cliente->datosImportancia}}">
 </div>
 <div class="form-group">
 <label for="tipo">Tipo</label>
 <input type="text" name="tipo" id="tipo" class="form-control" value="{{$cliente->tipo}}">
 </div>
  <div class="form-group">
 <label for="dias">Dias disponibles</label>
 <input type="number" name="dias" id="dias" class="form-control" min="0" max="7" value="{{$cliente->dias}}">
 </div>
 <h4>Dieta</h4>
 <div class="form-group">
 <label for="desayuno">Desayuno</label>
 <textarea name="desayuno" id="desayuno" class="form-control" rows="3">{{$cliente->getDesayuno($cliente->idComida)}}</textarea>
 </div>
 <div class="form-group">
 <label for="aperitivo">Aperitivo</label>
 <textarea name="aperitivo" id="aperitivo" class="form-control" rows="3">{{$cliente->getAperitivo($cliente->idComida)}}</textarea>
 </div>
 <div class="form-group">
 <label for="comida">Comida</label>
 <textarea name="comida" id="comida" class="form-control" rows="3">{{$cliente->getComida($cliente->idComida)}}</textarea>
 </div>
 <div class="form-group">
 <label for="merienda">Merienda</label>
 <textarea name="merienda" id="merienda" class="form-control" rows="3">{{$cliente->getMerienda($cliente->idComida)}}</textarea>
 </div>
 <div class="form-group">
 <label for="cena">Cena</label>
 <textarea name="cena" id="cena" class="form-control" rows="3">substr({{$cliente->getCena($cliente->idComida)}},10,12)</textarea>
 </div>
 <div class="form-group">
  <label for="imagen">Imagen</label>
 <input type="file" id="imagen"  name="imagen" class="form-control"></input>
 </div>
 <div class="form-group text-center">
 <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
 Editar cliente
 </button>
 </div>
  </form>
 </div>
 </div>
 </div>
</div>
@endsection