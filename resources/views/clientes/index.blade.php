@extends("layouts.master")

@section("titulo")
OnlineTrainer
@endsection
@section("contenido") 
@if(session('mensaje'))
	<div class="alert alert-danger">
		{{ session('mensaje')}}
	</div>
@endif
<div class="row">
	@foreach( $arrayClientes as $cliente )
		<div class="col-xs-12 col-sm-6 col-md-4 ">
			<a href="{{ url('/clientes/ver/' . $cliente->id) }}">
				<img src="{{asset('assets/imagenes')}}/{{$cliente->fotoCara}}" style="height:200px"/>
				<h4 style="min-height:45px;margin:5px 0 10px 0">
				</h4>
				<p>Creado: {{$cliente->created_at}}</p>
				<p>Nombre: {{$cliente->nombre}}</p>
				<p>Apellidos: {{$cliente->apellidos}}</p>
				</h6>
			</a>
		</div>
	@endforeach
</div>
@endsection