<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([ "middleware" => "auth"],function (){

Route::get('/','InicioController@getInicio');

Route::get('clientes','ClientesController@getTodas');

Route::get('clientes/ver/{id}','ClientesController@getVer'); 

Route::get('clientes/crear','ClientesController@getCrear');

Route::post('clientes/crear','ClientesController@postCrear');

Route::get('clientes/editar/{id}','ClientesController@getEditar');

Route::post('clientes/crear','ClientesController@postCrear');

Route::post('clientes/editar/{id}','ClientesController@postEditar');

});

Auth::routes();
