<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Cliente;
use App\Comidas;

class DatabaseSeeder extends Seeder
{
    private $arrayCliente = array(
		array(
			'nombre' => 'Luis',
			'apellidos' => 'Martinez',
			'tipo' => 'Dieta',
			'datosImportancia' => 'Alergica a los frutos secos',
			'objetivo' => 'Perder 10kg',
			'fotoCara' => 'cara1.jpg',
			'dias' => 3,
			'idComida' => 1,

		),
		array(
			'nombre' => 'Jimena',
			'apellidos' => 'Rodríguez',
			'tipo' => 'Dieta',
			'datosImportancia' => 'Celiaco',
			'objetivo' => 'Ganar masa muscular',
			'fotoCara' => 'cara2.jpg',
			'dias' => 2,
			'idComida' => 2,
		),
	);
	private $comidas = array(
		array(
			'desayuno' => '-2 tostadas de pan integrales/-cafe con leche sin azucar',
			'aperitivo' => '-sandwich de 100g de jamon',
			'comida' => '-300g de arroz/-200g de pollo a la plancha',
			'merienda' => '-Manzana o un pera',
			'cena' => '-200g de ensalda/-Tortilla francesa de 2 huevos de atun',  
		),
		array(
			'desayuno' => '-200g de leche/-100g de cereales integrales',
			'aperitivo' => '-sandwich con 100g de queso fresco',
			'comida' => '-300g de filete de ternera/-100g de ensalada de tomate',
			'merienda' => '-Pera/-Yogur natural',
			'cena' => '-200g de pavo/-150g de pan bimbo',						
		)
	);
	 public function run()
    {
        self::seedClientes();
        self::seedUsers();
        self::seedComidas();
        $this->command->info('Tabla clientes,alimentos y dieta inicializada con datos');
    }
    private function seedClientes()
    {
    	DB::table('clientes')->delete();
    	foreach ($this->arrayCliente as $cliente) {

    		$m = new Cliente();
    		$m->nombre=$cliente['nombre'];
    		$m->apellidos=$cliente['apellidos'];
    		$m->tipo=$cliente['tipo'];
    		$m->datosImportancia=$cliente['datosImportancia'];
    		$m->objetivo=$cliente['objetivo'];
    		$m->fotoCara=$cliente['fotoCara'];
    		$m->dias=$cliente['dias'];
    		$m->idComida=$cliente['idComida'];
    		$m->save();
    	}
    }
    private function seedUsers()
    {
    	DB::table('users')->delete();

    	$user = new User();
    	$user->name="Anibal";
    	$user->email="anibal@hotmail.com";
    	$user->password= bcrypt("usuario@1");
    	$user->save();
    }
    private function seedComidas()
    {
        DB::table('comidas')->delete();
        foreach ($this->comidas as $comida1) {

            $cmd = new Comidas();
            $cmd->desayuno=$comida1['desayuno'];
            $cmd->aperitivo=$comida1['aperitivo'];
            $cmd->comida=$comida1['comida'];
            $cmd->merienda=$comida1['merienda'];
            $cmd->cena=$comida1['cena'];
            $cmd->save();
        }
    }

}
