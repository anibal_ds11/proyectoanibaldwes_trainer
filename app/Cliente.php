<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Cliente extends Model
{
    public function getDesayuno($id)
    {
    	 $comida = Comidas::where('id',$id)->get('desayuno');
    	 return $comida;
    	 
    }
    public function getAperitivo($id)
    {
    	 $comida = Comidas::where('id',$id)->get('aperitivo');
    	 return $comida;
    	 
    }
    public function getComida($id)
    {
    	 $comida = Comidas::where('id',$id)->get('comida');
    	 return $comida;
    	 
    }
    public function getMerienda($id)
    {
    	 $comida = Comidas::where('id',$id)->get('merienda');
    	 return $comida;
    	 
    }
    public function getCena($id)
    {
    	 $comida = Comidas::where('id',$id)->get('cena');
    	 return $comida;
    	 
    }
    public function getComidas()
    {
    	return $this->hasMany('App\Comidas','cliente','idComida');
    }


}
    
/*        public function getComidas()
    {
    	return $this->hasMany('App\Comidas','id','id');
    }
    */
