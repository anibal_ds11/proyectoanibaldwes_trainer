<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\Comidas;

class ClientesController extends Controller
{
   public function getTodas()
    {
    	return view("clientes.index",array('arrayClientes'=>Cliente::all()));
    }
    public function getVer($id)
    {
    	return view('clientes.mostrar',array('cliente'=>Cliente::findOrFail($id)));
    }
    public function getCrear()
    {
    	return view('clientes.crear');
    }
    public function getEditar($id)
    {
    	return view('clientes.editar',array('cliente'=>Cliente::findOrFail($id)));
    }
    public function postCrear(Request $request)
    {
    	$comidas = new Comidas();
        $comidas->desayuno = $request->desayuno;
        $comidas->aperitivo = $request->aperitivo;
        $comidas->comida= $request->comida;
        $comidas->merienda = $request->merienda;
        $comidas->cena = $request->cena;

            try{
            $comidas->save();
        }catch(\Illuminate\Database\QueryException $ex){
            return redirect('clientes')->with('mensaje','Fallo de dieta');
        }

        $cliente = new Cliente();
    	$cliente->nombre = $request->nombre;
    	$cliente->apellidos = $request->apellidos;
    	$cliente->tipo = $request->tipo;
    	$cliente->datosImportancia= $request->datosImportancia;
    	$cliente->objetivo = $request->objetivo;
    	$cliente->dias = $request->dias;
        $cliente->fotoCara = $request->imagen->store('','clientes');
        $cliente->idComida= $comidas->id;
    	try{
            $cliente->save();
    		return redirect('clientes')->with('mensaje','Cliente insertado');
    	}catch(\Illuminate\Database\QueryException $ex){
    		return redirect('clientes')->with('mensaje','Fallo al crear el cliente');
    	}
    }
    public function postEditar(Request $request,$id)    
    {    

        //La comida no me la guarda bien, debido a que la cojo de la consulta y al guardarla, se me guarda todo
    	$cliente = Cliente::where('id',$id)->first();    	
        $comida = Comidas::where('id',$cliente->idComida)->first();
        
        $cliente->nombre = $request->nombre;        
        $cliente->apellidos = $request->apellidos;
        $cliente->tipo = $request->tipo;
        $cliente->datosImportancia= $request->datosImportancia;
        $cliente->objetivo = $request->objetivo;
        $cliente->dias = $request->dias;
        $cliente->idComida = $comida->id;

        $comida->desayuno = $request->desayuno;
        $comida->aperitivo = $request->aperitivo;
        $comida->comida= $request->comida;
        $comida->merienda = $request->merienda;
        $comida->cena = $request->cena;
    	
        if ($request->has('fotoCara')) {
    		$cliente->fotoCara = $request->imagen->store('','clientes');
    	}

    	try{
    	$cliente->save();
    	return redirect('clientes')->with('mensaje','Cliente editado');
    	}catch(\Illuminate\Database\QueryException $ex){
    		return redirect('clientes')->with('mensaje','Fallo al editar el cliente');
    	}
    }
}
